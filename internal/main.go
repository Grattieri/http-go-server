// main.go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

var path string
var folder string
var log_file = "recipes.log"

func current_path() string {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)
	return path
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Recepe V0.1")
	log.Println("Endpoint Hit: homePage")
}

func saveRecepy(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var raw map[string]interface{}
	json.Unmarshal(reqBody, &raw)
	file_name := path + folder + raw["file_name"].(string)
	log.Println("file_name =", file_name)
	data := raw["data"]
	file, _ := json.MarshalIndent(data, "", " ")
	_ = ioutil.WriteFile(file_name, file, 0644)
	log.Println("Saved to ", file_name)
	for key, value := range data.(map[string]interface{}) {
		log.Println(key, "=", value, " ")
	}
	json.NewEncoder(w).Encode("saved")
}

func loadRecepy(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var raw map[string]interface{}
	json.Unmarshal(reqBody, &raw)
	file_name := path + folder + raw["file_name"].(string)
	log.Println("file_name =", file_name)
	data := raw["data"]
	dat, _ := ioutil.ReadFile(file_name)
	json.Unmarshal(dat, &data)
	log.Println(string(dat))
	log.Println("Loaded from ", file_name)
	json.NewEncoder(w).Encode(data)
}

func ReadDir(dirname string) ([]os.FileInfo, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	list, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return nil, err
	}
	sort.Slice(list, func(i, j int) bool { return list[i].Name() < list[j].Name() })
	return list, nil
}

func browseRecepies(w http.ResponseWriter, r *http.Request) {
	log.Println("Browse")

	reqBody, _ := ioutil.ReadAll(r.Body)
	var raw map[string]interface{}
	json.Unmarshal(reqBody, &raw)
	folder = raw["folder"].(string)
	log.Println("folder =", folder)
	log.Println("Browsed folder ", folder)

	f, _ := os.Open(path + folder + ".")
	files, _ := f.Readdir(-1)
	var l []string
	for _, file := range files {
		//log.Println(file.Name())
		l = append(l, file.Name())
	}
	log.Println(l)

	json.NewEncoder(w).Encode(l)
}

func handleRequests() {
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, // All origins
	})

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/save", saveRecepy).Methods("POST")
	myRouter.HandleFunc("/load", loadRecepy).Methods("POST")
	myRouter.HandleFunc("/browse", browseRecepies).Methods("POST")
	myRouter.HandleFunc("/", homePage)
	log.Println("Listening on 5000")
	fmt.Println("Listening on 5000")
	log.Fatal(http.ListenAndServe(":5000", c.Handler(myRouter)))
}

func main() {
	file, err := os.OpenFile(log_file, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	log.SetOutput(file)

	path = current_path()
	log.Print("Current path", path)
	handleRequests()
}
