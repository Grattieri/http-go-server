// Version Beta 0.1
// Massimo Grattieri 2020


var data ={};
var file_name = "";

$(document).ready(function() {
    console.log("Start");
    get_data();
    //data['Counter'] = Number(data['Counter']) + 1;
    set_data();
    add_commands();
    browse_data_request();
});

var add_commands = function() {
    $("#recipe_save").click(function(){
        var file_name = $("#recipe_file_name")[0].value ;
        console.log("save to ", file_name );
        get_data();
        save_data_request(file_name, data );
    });

    $("#recipe_send").click(function(){
        var ip = $("#recipe_ip")[0].value ;
        console.log("send to ", ip );
        //get_data();
        //save_data_request(file_name, data );
    });

    $("#recipe_fetch").click(function(){
        var ip = $("#recipe_ip")[0].value ;
        console.log("fetch from  ", ip );
        //set_data();
        //save_data_request(file_name, data );
    });


    $("#recipe_load").click(function(){
        var file_name = $("#recipe_file_name")[0].value ;
        console.log("load from ", file_name )
        load_data_request(file_name);
        set_data();
    });

}

var get_data=function(){
    $('[recipe_data]').each( function( index ) {
        var recepy_data = this.getAttribute("recipe_data");
        data[recepy_data]= this.value;
    });
}

var set_data=function(){
    console.log("set data ", data )
    $('[recipe_data]').each( function( index ) {
        this.value = "?";
        var recepy_data = this.getAttribute("recipe_data");
        this.value = data[recepy_data];
    });
}

var save_data_request=function( file_name, data  ){
    console.log("save data to ", file_name)
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          console.log(this.responseText);
          browse_data_request();
      }
    };
    xhttp.open("POST", "http://localhost:5000/save", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    data['Timestamp'] = new Date().toLocaleString();
    if (data['Counter'] == null) { data['Counter'] = 0; }
    data['Counter'] = data['Counter'] + 1;
    payload = JSON.stringify({ file_name: file_name, "data": data });
    console.log("save requested payload ", payload )
    xhttp.send(payload);

}

var load_data_request=function( file_name  ){
    console.log("load data from ", file_name)
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          console.log("response", this.responseText);
          j=JSON.parse(this.responseText)
          console.log("json response" , j)
          for(var key in j) {
            data[key]=data.value=j[key]
          };
          console.log("updated data ", data )
          set_data();
      }
    };
    xhttp.open("POST", "http://localhost:5000/load", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    payload = JSON.stringify({ file_name: file_name, "data": {} });
    console.log("load requested payload ", payload )
    xhttp.send(payload);
}

var browse_data_request=function(){
    console.log("browse folder ")
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          console.log("response", this.responseText);
          j=JSON.parse(this.responseText)
          console.log("response", j);
          populate_list(j);
      }
    };
    xhttp.open("POST", "http://localhost:5000/browse", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    payload = JSON.stringify({ folder: "/recipes/", "data": {} });
    console.log("browse requested payload ", payload )
    xhttp.send(payload);

}

var populate_list = function( myOptions ){
    var mySelect = $('#recipe_browse');
    mySelect.empty()

    $.each(myOptions.sort(), function(val, text) {
        mySelect.append(
            $('<li></li>').val(val).html("<a onclick='selected(this)'>" + text + "</a>")
        );
    });
}

var selected = function(obj){
    console.log("selected", obj.innerHTML );
    $("#recipe_file_name")[0].value = obj.innerHTML;
    load_data_request(obj.innerHTML);

}