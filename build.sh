export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin




echo $GOPATH
echo $PATH

export GOARCH=amd64
export GOOS=linux
echo $GOARCH $GOOS
go build  -v -o bin/recepy-linux-amd64 internal/main.go

export GOARCH=arm64
export GOOS=linux
echo $GOARCH $GOOS
go build  -v -o bin/recepy-linux-arm64 internal/main.go

export GOARCH=arm32
export GOOS=linux
echo $GOARCH $GOOS
go build  -v -o bin/recepy-linux-arm32 internal/main.go

export GOARCH=amd64
export GOOS=darwin
echo $GOARCH $GOOS
go build  -v -o bin/recepy-darwin-amd64 internal/main.go

export GOARCH=386
export GOOS=windows
echo $GOARCH $GOOS
go build  -v -o bin/recepy-win-386 internal/main.go

export GOARCH=amd64
export GOOS=windows
echo $GOARCH $GOOS
go build  -v -o bin/recepy-win-amd64 internal/main.go






