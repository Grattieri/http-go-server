module gitlab.com/Grattieri/http-go-server

go 1.14

require (
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/gorilla/mux v1.7.4
	github.com/rs/cors v1.7.0
)
