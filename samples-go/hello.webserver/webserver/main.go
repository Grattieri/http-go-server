package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"

)

func main() {
	port := flag.String("p", "8080", "port on which to serve web interfaces")
	flag.Usage = usage
	flag.Parse()

	if len(flag.Args()) != 1 {
		flag.Usage()
		log.Fatalf("Directory to serve relative to the snap root directory is the only required argument")
	}

	snapdir := os.Getenv("SNAP")
	snapcommon := os.Getenv("SNAP_COMMON")
	www := flag.Arg(0)
	commonpath := path.Join(snapcommon, "solutions/activeConfiguration/www")
	fmt.Printf("Check if directory in %s exists \n", commonpath)
	if _, err := os.Stat((path.Join(snapcommon,"solutions/activeConfiguration/www"))); os.IsNotExist(err) {
		www = path.Join(snapdir, flag.Arg(0))
		fmt.Printf("activeConfiguration/www does not exist \n")
		fmt.Printf("%s \n ", www)
	} else {
		www = path.Join(snapcommon, "solutions/activeConfiguration/www", flag.Arg(0))
		fmt.Printf("activeConfiguration exist \n")
		fmt.Printf(www)
	}
	fmt.Printf("Directory to serve: ")
	fmt.Println(www)
	
	// To serve a directory on disk (/tmp) under an alternate URL
	// path (/tmpfiles/), use StripPrefix to modify the request
	// URL's path before the FileServer sees it:
	http.Handle("/hello-webserver/", http.StripPrefix("/hello-webserver/", http.FileServer(http.Dir(www))))
	panic(http.ListenAndServe(":"+*port, nil))
	
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s: [-p port] dir_to_serve\n", os.Args[0])
	flag.PrintDefaults()
}
